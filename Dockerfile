FROM alpine
WORKDIR /work
RUN apk add openssl curl bash git && \
    curl https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash -s -- -a && \
    mv /root/yandex-cloud/bin/yc /usr/local/bin/yc && \
    curl -LO https://dl.k8s.io/release/v1.29.2/bin/linux/amd64/kubectl && \
    chmod +x ./kubectl && \
    mv ./kubectl /usr/local/bin/kubectl && \
    curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 && \
    chmod 700 get_helm.sh && \
    ./get_helm.sh && \
    rm ./get_helm.sh
CMD ["/bin/bash"]